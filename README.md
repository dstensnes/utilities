# Utilities
This project is a collection of utilities for different purposes

## bin/firefox
This is a script for creating firefox profiles for individual sites. It also
has support for creating chromeless (no borders, url bar or tab bar) browser
windows. Tabs still work, but the actual tab bar is hidden. Ctrl-Tab,
middle-click etc still works though.
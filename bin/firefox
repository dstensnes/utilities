#! /bin/sh

##### DESCRIPTION #####
# This is a small wrapper script for running firefox with multiple profiles.
# By default it just wraps the normal firefox executable functionality, but
# appends a path to a profile named "default".
#
# You can create additional profiles like this:
#    firefox normal netflix 'https://www.netflix.com/'
#
# It also has support for creating profiles somewhat similar to 
# google chrome's app-mode functionality (which works
# great for netflix by the way!). All it does is hide
# the URL-bar and tabs, but keyboard shortcuts still works
# (like Ctrl+T for new tab, and Ctrl+Tab to cycle tabs,
# Ctrl+W to close a tab). Create an app profile like this:
#    firefox app netflix 'https://www.netflix.com/'
#
# Everything else is passed along to the real firefox
# executable like normal, so options etc should work
#
# Notes:
#   * I preseed a lot of settings in new profiles to prevent UI tours
#     and data sharing popups. I have however tried to disable all kind
#     of datasharing and telemetry, but further updates to firefox may
#     require additional settings to turn them off in the future.
#   * To delete a profile, delete the folder inside "$HOME/$PROFILE_FOLDERS_DIR"
#     and the corresponding wrapper script created inside "$LAUNCHER_SCRIPT_PATH"
#   * Clicking the Alt button once allows access to preferences and addons
#     from the old application menu, in case you need to install additional
#     addons or do other tweaks.
#
# Licence: Public domain without restrictions.
# 
# Warnings:
#   * As usual, use at your own risk. It works for me, it should hopefully
#     work for you if you follow the instructions above. I accept no
#     responsibility for bugs here.
#
# Enjoy!
#
# - Daniel Stensnes
#

#
#
##### CONFIGURATION #####
#
# Path to Firefox executable
FIREFOX_EXECUTABLE="$HOME/.local/apps/firefox/firefox"

# Folder to hold all firefox profiles. Always relative to home directory! No absolute urls! No trailing slash!
PROFILE_FOLDERS_DIR=".firefox-profiles"

# Space separated list of URLs for extensions to install in all profiles
EXTENSIONS_URL_LIST='https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/'

# Path where a launcher script for starting this profile will be created
LAUNCHER_SCRIPT_PATH="$HOME/.local/bin"


#
#
##### SCRIPT CODE #####
#
syntax () {
	echo "SYNTAX 1: $0 \"normal\" <appname> <url>"
	echo "SYNTAX 2: $0 \"app\" <appname> <url>"
	echo "SYNTAX 3: $0 [normal-firefox-options] <url>"
	echo "SYNTAX 4: $0"
}

# Validate that "$1" looks somewhat like a valid URL
checkIsUrl () {
	if [ -z "$(echo "$1" | grep "://")" ] ; then
		echo "No URL given. Follow the syntax!"
		syntax
		exit 1
	fi
}

# Preseed some settings in new profiles, to prevent
# UI tours and other popups on every new profile.
mkUserPref () {
echo '
pref("app.shield.optoutstudies.enabled", true);
pref("browser.bookmarks.restore_default_bookmarks", false);
pref("browser.contentblocking.introCount", 20);
pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
pref("browser.newtabpage.activity-stream.telemetry", false);
pref("browser.newtabpage.activity-stream.telemetry.ping.endpoint", "");
pref("browser.newtabpage.enabled", false);
pref("browser.newtabpage.enhanced", false);
pref("browser.newtabpage.storageVersion", 1);
pref("browser.onboarding.enabled", false);
pref("browser.onboarding.notification.finished", true);
pref("browser.onboarding.state", "watermark");
pref("browser.onboarding.notification.finished", true);
pref("browser.onboarding.tour-type", "none");
pref("browser.onboarding.tour.onboarding-tour-addons.completed", true);
pref("browser.onboarding.tour.onboarding-tour-customize.completed", true);
pref("browser.onboarding.tour.onboarding-tour-default-browser.completed", true);
pref("browser.onboarding.tour.onboarding-tour-performance.completed", true);
pref("browser.onboarding.tour.onboarding-tour-private-browsing.completed", true);
pref("browser.onboarding.tour.onboarding-tour-screenshots.completed", true);
pref("browser.link.open_newwindow.restriction", 0);
pref("browser.ping-centre.telemetry", false);
pref("browser.shell.checkDefaultBrowser", false);
pref("browser.shell.didSkipDefaultBrowserCheckOnFirstRun", true);
pref("browser.startup.homepage_override.mstone", "ignore");
pref("browser.uitour.enabled", false);
pref("datareporting.healthreport.uploadEnabled", false);
pref("extensions.pocket.enabled", false);
pref("extensions.shield-recipe-client.enabled", false);
pref("extensions.shield-recipe-client.first_run", false);
pref("extensions.shield-recipe-client.startupExperimentPrefs.browser.newtabpage.activity-stream.enabled", false);
pref("extensions.ui.dictionary.hidden", true);
pref("extensions.ui.experiment.hidden", true);
pref("extensions.ui.locale.hidden", true);
pref("network.cookie.cookieBehavior", 3);
pref("network.dns.disablePrefetch", true);
pref("privacy.donottrackheader.enabled", true);
pref("privacy.clearOnShutdown.cookies", false);
pref("privacy.clearOnShutdown.downloads", false);
pref("privacy.clearOnShutdown.formdata", false);
pref("privacy.clearOnShutdown.history", false);
pref("privacy.clearOnShutdown.sessions", false);
pref("privacy.donottrackheader.enabled", true);
pref("privacy.history.custom", true);
pref("privacy.sanitize.didShutdownSanitize", true);
pref("privacy.sanitize.sanitizeOnShutdown", true);
pref("security.ssl.errorReporting.url", "");
pref("toolkit.telemetry.archive.enabled", false);
pref("toolkit.telemetry.bhrPing.enabled", false);
pref("toolkit.telemetry.enabled", false);
pref("toolkit.telemetry.firstShutdownPing.enabled", false);
pref("toolkit.telemetry.newProfilePing.enabled", false);
pref("toolkit.telemetry.reportingpolicy.firstRun", false);
pref("toolkit.telemetry.server", "");
pref("toolkit.telemetry.shutdownPingSender.enabled", false);
pref("toolkit.telemetry.unified", false);
pref("toolkit.telemetry.updatePing.enabled", false);
pref("browser.uidensity", 1);
pref("lightweightThemes.selectedThemeID", "firefox-compact-dark@mozilla.org");
pref("toolkit.cosmeticAnimations.enabled", false);
pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 20);
pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1");
pref("widget.content.gtk-theme-override", "Adwaita:light");
' > $1
}

mkProfilePath () {
	if [ "$1" != 'app' -a "$1" != 'normal' ] ; then
		echo "ERROR: No app-mode given to mkProfilePath"
		exit 1
	fi

	if [ -z "$2" -o -n "$(echo "$2" | grep '://')" ] ; then
		echo "ERROR: No profile name given to mkProfilePath"
		exit 1
	fi

	if [ -z "$3" ] ; then
		echo "WARNING: No default URL given to mkProfilePath. Not setting a default"
	fi

	# Build SCRIPT_FIREFOX_PATH from FIREFOX_EXECUTABLE but replace home directory
	# with $HOME if found at the beginning (helps with differing usernames)
	SCRIPT_FIREFOX_PATH="$FIREFOX_EXECUTABLE"
	HOMEDIR_STRING_LENGTH="$(echo -n "$HOME/" | wc -c)"
	if [ "$(echo -n "$SCRIPT_FIREFOX_PATH" | cut -b1-$HOMEDIR_STRING_LENGTH)" = "$HOME/" ] ; then
		SCRIPT_FIREFOX_PATH="\$HOME$(echo "$SCRIPT_FIREFOX_PATH" | cut -b$HOMEDIR_STRING_LENGTH-)"
	fi

	PROFILE_PATH="$2"
	PROFILE_NAME="$(echo "$2" | sed 's@/\+$@@' | xargs basename)"

	# Create folder to contain the firefox profile
	if [ ! -d "$PROFILE_PATH" ] ; then
		mkdir -p "${PROFILE_PATH}"
	fi

	# Seed nice to have settings
	if [ ! -f "$PROFILE_PATH/user.js" ] ; then
		mkUserPref "$PROFILE_PATH/user.js"
	fi

	# If mode=="app", add chrome customizations to remove toolbars and tabs
	if [ "$1" = "app" ] ; then
		mkdir -p "${PROFILE_PATH}/chrome"
		chmod og-rwx -R "${PROFILE_PATH}"
		echo $ECHO_ESCAPE_CHAR_OPTION '#TabsToolbar,#nav-bar { visibility: collapse !important; }'>"${PROFILE_PATH}/chrome/userChrome.css"
		echo $ECHO_ESCAPE_CHAR_OPTION '@-moz-document url-prefix("about:") { * { color: #F9F9FA !important; background-color: #0C0C0C !important; } }'>${PROFILE_PATH}/chrome/userContent.css
	fi

	# Generate a launcher script inside $LAUNCHER_SCRIPT_PATH that will launch the profile
	if [ -n "$LAUNCHER_SCRIPT_PATH" -a ! -f "$HOME/$LAUNCHER_SCRIPT_PATH/$2" -a "$3" != 'no-script' ] ; then
		if [ -n "$3" -a "$3" != 'none' ] ; then
			echo $ECHO_ESCAPE_CHAR_OPTION '#! /bin/sh\nexport GTK_THEME=Adwaita:light\nexec '"$SCRIPT_FIREFOX_PATH"' --class="'$PROFILE_NAME'" -profile "$HOME/'"$PROFILE_FOLDERS_DIR/$PROFILE_NAME"'" -no-remote "'"$3"'" &> $HOME/.output.firefox-'"$PROFILE_NAME"'' > "$LAUNCHER_SCRIPT_PATH/$PROFILE_NAME"
		else
			echo $ECHO_ESCAPE_CHAR_OPTION '#! /bin/sh\nexport GTK_THEME=Adwaita:light\nexec '"$SCRIPT_FIREFOX_PATH"' --class="'$PROFILE_NAME'" -profile "$HOME/'"$PROFILE_FOLDERS_DIR/$PROFILE_NAME"'" -no-remote $* &> $HOME/.output.firefox-'"$PROFILE_NAME"'' > "$LAUNCHER_SCRIPT_PATH/$PROFILE_NAME"
		fi
		chmod 755 "$LAUNCHER_SCRIPT_PATH/$PROFILE_NAME"
	fi
}

installExtensions () {
	if [ -n "$EXTENSIONS_URL_LIST" ] ; then
		echo $EXTENSIONS_URL_LIST | xargs $FIREFOX_EXECUTABLE -profile "$1" -no-remote &> /dev/null
	fi
}




##### MAIN 

# Determine if we need "-e" for "echo" to process escape sequences
if [ -n "$(readlink "/proc/$$/exe" | grep '/bash')" ] ; then
	ECHO_ESCAPE_CHAR_OPTION="-e"
else
	ECHO_ESCAPE_CHAR_OPTION=""
fi

# Verify that $FIREFOX_EXECUTABLE points to a valid executable
if [ ! -x "$FIREFOX_EXECUTABLE" ] ; then
	echo "FIREFOX_EXECUTABLE does not point to an executable file (firefox executable)"
	exit 1
fi

# Ensure that $LAUNCHER_SCRIPT_PATH is not empty and points to a folder
[ -n "$LAUNCHER_SCRIPT_PATH" -a ! -d "$LAUNCHER_SCRIPT_PATH" ] && mkdir -p "$LAUNCHER_SCRIPT_PATH" 

# Set umask to ensure that noone else will be allowed to access files and
# folders created from this script
umask 077

# Mode of operation switching logic
PROFILE_TYPE="$1"
case "$PROFILE_TYPE" in
	app|normal)
		PROFILE_NAME="$2"
		PROFILE_PATH="$HOME/$PROFILE_FOLDERS_DIR/$PROFILE_NAME/"
		PROFILE_URL="$3"
		checkIsUrl "$PROFILE_URL"
		mkProfilePath "$PROFILE_TYPE" "$PROFILE_PATH" "$PROFILE_URL"
		installExtensions "$PROFILE_PATH"
		;;
	help)
		syntax
		;;
	*)
		PROFILE_PATH="$HOME/$PROFILE_FOLDERS_DIR/default/"

		# Create default profile if it does not exist already
		if [ ! -d "$PROFILE_PATH" ] ; then
			mkProfilePath normal "$PROFILE_PATH" no-script
		fi

		# Start firefox with GTK_THEME fix to prevent gtk-themes to leak
		# into firefox HTML rendering engine, causing unreadable text when
		# gtk is configured with a dark theme
		export GTK_THEME=Adwaita:light
		exec ${FIREFOX_EXECUTABLE} -profile "${PROFILE_PATH}" $* &> $HOME/.output.firefox-default
		;;
esac
